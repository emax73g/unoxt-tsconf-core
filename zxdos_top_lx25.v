`timescale 1ns / 1ps
`default_nettype none

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:24:02 08/15/2016 
// Design Name: 
// Module Name:    tld_test_prod_v4 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
// 
//////////////////////////////////////////////////////////////////////////////////
module zxdos_tsconf_lx25 (
   input wire clk50mhz,
   //---------------------------
   inout wire clkps2,
   inout wire dataps2,
   //---------------------------
   inout wire mousedata,
   inout wire mouseclk,
   //---------------------------
   input wire ear,
   //---------------------------
   output wire [20:0] sram_addr,
   inout wire [15:0] sram_data,
   output wire sram_we_n,
   output wire sram_ub_n,
	output wire sram_lb_n,
   //---------------------------
   output wire joy_load_n,
   output wire joy_clk,
   input wire joy_data,
   //---------------------------
   output wire testled,
   //---------------------------
   output wire sd_cs_n,
   output wire sd_clk,
   output wire sd_mosi,
   input wire sd_miso,
   //---------------------------
   output wire flash_cs_n,
   output wire flash_clk,
   output wire flash_mosi,
   input wire flash_miso,
   //---------------------------
//   output wire sdram_clk,          // seales validas en flanco de suida de CK
//   output wire sdram_cke,
//   output wire sdram_dqmh_n,      // mascara para byte alto o bajo
//   output wire sdram_dqml_n,      // durante operaciones de escritura
//   output wire [12:0] sdram_addr, // pag.14. row=[12:0], col=[8:0]. A10=1 significa precharge all.
//   output wire [1:0] sdram_ba,    // banco al que se accede
//   output wire sdram_cs_n,
//   output wire sdram_we_n,
//   output wire sdram_ras_n,
//   output wire sdram_cas_n,
//   inout tri [15:0] sdram_dq,
   //---------------------------
   output wire [5:0] r,
   output wire [5:0] g,
   output wire [5:0] b,
   output wire hsync,
   output wire vsync,
   output wire audio_out_left,
   output wire audio_out_right
   );

   //assign joy_load_n = 1'b1;
	//assign joy_clk = 1'b1;
	assign testled = 1'b0;
	assign flash_cs_n = 1'b1;
	assign flash_clk = 1'b1;
	assign flash_mosi = 1'b1;
	//assign audio_out_left = 1'b1;
	//assign audio_out_right = 1'b1;
	
	wire clk_sys;
   wire locked;
   pll pll (
	  .CLK_IN1(clk50mhz),
	  .CLK_OUT1(clk_sys),
	  .LOCKED(locked)
	);
	
	reg ce_28m;
	reg [1:0] div = 2'd0;
	always @(negedge clk_sys) 
	begin
		div <= div + 1'd1;
		if(div == 2) div <= 0;
		ce_28m <= !div;
	end
	
	wire [7:0] video_r;
	wire [7:0] video_g;
	wire [7:0] video_b;

	reg  [11:0] joyAmd;
	wire [11:0] joyAmd_i;
	wire [11:0] joyBmd;
	
	wire [2:0] turbo_keys;
	
	tsconf tsconf (
     .clk(clk_sys),
	  .ce(ce_28m),
	  
	  .locked(locked),
	  .sram_addr(sram_addr),
	  .sram_data(sram_data),
     .sram_we_n(sram_we_n),
     .sram_ub_n(sram_ub_n),
     .sram_lb_n(sram_lb_n),
     .VGA_R(video_r),
     .VGA_G(video_g),
     .VGA_B(video_b),
     .VGA_HS(hsync),
     .VGA_VS(vsync),	  
     .beep(),
     .sdcs_n(sd_cs_n),
     .sdclk(sd_clk),
     .sddo(sd_mosi),
     .sddi(sd_miso),
	  .audio_out_l(audio_out_left),
	  .audio_out_r(audio_out_right),
     .clkps2(clkps2),
     .dataps2(dataps2),
     .mouseclk(mouseclk),
     .mousedata(mousedata),
	  .joyA ({~(joyAmd[7:4]&{1'b1,turbo_keys}),~joyAmd[0],~joyAmd[1],~joyAmd[2],~joyAmd[3]})
	 );

///	joy_select <= hsync_aux;
   
  joydecoder #(.FRECCLKIN(84), .FRECCLKOUT(16)) joydecoder
  (
     .clk        (clk_sys),
     .joy_data   (joy_data),
     .joy_clk    (joy_clk),
     .joy_load_n (joy_load_n), 
     .reset      (!locked),
     .hsync_n_s  (hsync),

     .joy1_o     (joyAmd_i),  // MXYZ SACB RLDU  Negative Logic
     .joy2_o     (joyBmd)   // MXYZ SACB RLDU  Negative Logic
   );   
  reg [3:0] frame_sync;
  reg frame;
  always @(posedge clk_sys)
  begin
    frame_sync <= {frame_sync[2:0],vsync};
	 if (frame_sync[3:2] == 2'b01) begin
	   frame <= !frame;
		joyAmd <= joyAmd_i;
    end
  end
  
  assign turbo_keys = {3{frame}} | {joyAmd[10],joyAmd[8],joyAmd[9]};
assign r[5:0] = video_r[7:2];
assign g[5:0] = video_g[7:2] ;
assign b[5:0] = video_b[7:2];
	 
//   multiboot vuelta_al_spectrum (
//    .clk_icap(clk7),
//    .REBOOT(master_reset)
//    );
endmodule
