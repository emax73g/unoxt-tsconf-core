`timescale 1ns / 1ps
`default_nettype none

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:24:02 08/15/2016 
// Design Name: 
// Module Name:    tld_test_prod_v4 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
// 
//////////////////////////////////////////////////////////////////////////////////
//UnoXT Generation Selector
`define unoxt
//`define unoxt2

`ifdef unoxt
	module unoxt_tsconf_top (
`elsif unoxt2
	module unoxt2_tsconf_top (
`else
	module unoxt_tsconf_top (
`endif

   input wire clk50mhz,
   //---------------------------
   inout wire clkps2,
   inout wire dataps2,
   //---------------------------
   inout wire mousedata,
   inout wire mouseclk,
   //---------------------------
   input wire ear,
   //---------------------------
   output wire [20:0] sram_addr,
   inout wire [15:0] sram_data,
	//Max
   output wire sram_ce_n,
   output wire sram_oe_n,

   output wire sram_we_n,
   output wire sram_ub_n,
	output wire sram_lb_n,
   //---------------------------
   //Max
	/*output wire joy_load_n,
   output wire joy_clk,
   input wire joy_data,
   */
   input wire joyp1_i,
   input wire joyp2_i,
   input wire joyp3_i,
   input wire joyp4_i,
   input wire joyp6_i,
   output wire joyp7_o,
   input wire joyp9_i,
	
	//---------------------------
   //Max
	//output wire testled,
   //---------------------------
   output wire sd_cs_n,
   output wire sd_clk,
   output wire sd_mosi,
   input wire sd_miso,
   //---------------------------
   output wire flash_cs_n,
   output wire flash_clk,
   output wire flash_mosi,
   input wire flash_miso,
	//Max
	output wire flash_wp_o,
	output wire flash_hold_o,
	
   //---------------------------
//   output wire sdram_clk,          // seales validas en flanco de suida de CK
//   output wire sdram_cke,
//   output wire sdram_dqmh_n,      // mascara para byte alto o bajo
//   output wire sdram_dqml_n,      // durante operaciones de escritura
//   output wire [12:0] sdram_addr, // pag.14. row=[12:0], col=[8:0]. A10=1 significa precharge all.
//   output wire [1:0] sdram_ba,    // banco al que se accede
//   output wire sdram_cs_n,
//   output wire sdram_we_n,
//   output wire sdram_ras_n,
//   output wire sdram_cas_n,
//   inout tri [15:0] sdram_dq,
   //---------------------------
   //Max
	output wire [4:0] r,
   output wire [4:0] g,
   output wire [4:0] b,
   
	output wire hsync,
   output wire vsync,
   output wire audio_out_left,
   output wire audio_out_right,
	//Max
	output wire ledRedO,
	output wire ledYellowO,
	output wire ledGreenO,
	output wire ledBlueO,
	output wire [8:0] test 
   );
	
	//Max
	`define DEFAULT_100_LEDS;
	
	`ifdef ROUND_DIF_LEDS
		//3mm round diffused LEDs
		localparam ledRedK = 16'd20;
		localparam ledYellowK = 16'd50;
		localparam ledGreenK = 16'd12;
		localparam ledBlueK = 16'd50;
	`elsif SQUARE_LEDS
		//square color LEDs
		localparam ledRedK = 16'd20;
		localparam ledYellowK = 16'd33;
		localparam ledGreenK = 16'd100;
		localparam ledBlueK = 16'd20;
	`else
		//default 100% LEDs
		localparam ledRedK = 16'd100;
		localparam ledYellowK = 16'd100;
		localparam ledGreenK = 16'd100;
		localparam ledBlueK = 16'd100;
	`endif

   //assign joy_load_n = 1'b1;
	//assign joy_clk = 1'b1;
	//Max
	//assign testled = 1'b1;
	
	assign flash_cs_n = 1'b1;
	assign flash_clk = 1'b1;
	assign flash_mosi = 1'b1;
	//Max
	assign flash_wp_o = 1'b1;
	assign flash_hold_o = 1'b1;
	
	//assign audio_out_left = 1'b1;
	//assign audio_out_right = 1'b1;
	
	wire clk_sys;
   wire locked;
   pll pll (
	  .CLK_IN1(clk50mhz),
	  .CLK_OUT1(clk_sys),
	  .LOCKED(locked)
	);
	
	reg ce_28m;
	reg [1:0] div = 2'd0;
	always @(negedge clk_sys) 
	begin
		div <= div + 1'd1;
		if(div == 2) div <= 0;
		ce_28m <= !div;
	end
	
	wire [7:0] video_r;
	wire [7:0] video_g;
	wire [7:0] video_b;

	reg  [11:0] joyAmd;
	wire [11:0] joyAmd_i;
	wire [11:0] joyBmd;
	
	wire [2:0] turbo_keys;
	
	//Max
	wire [11:0] joyA;
	wire [1:0] turbo;
	
	tsconf tsconf (
     .clk(clk_sys),
	  .ce(ce_28m),
	  
	  .locked(locked),
	  .sram_addr(sram_addr),
	  .sram_data(sram_data),
     .sram_we_n(sram_we_n),
     .sram_ub_n(sram_ub_n),
     .sram_lb_n(sram_lb_n),
     .VGA_R(video_r),
     .VGA_G(video_g),
     .VGA_B(video_b),
	  .VGA_HS(hsync),
     
	  .VGA_VS(vsync),	  
     .beep(),
     .sdcs_n(sd_cs_n),
     .sdclk(sd_clk),
     .sddo(sd_mosi),
     .sddi(sd_miso),
	  .audio_out_l(audio_out_left),
	  .audio_out_r(audio_out_right),
     .clkps2(clkps2),
     .dataps2(dataps2),
     .mouseclk(mouseclk),
     .mousedata(mousedata),
	  //Max
	  //.joyA ({~(joyAmd[7:4]&{1'b1,turbo_keys}),~joyAmd[0],~joyAmd[1],~joyAmd[2],~joyAmd[3]})
	  //.joyA(~{6'b111111, joyp9_i, joyp6_i, joyp1_i, joyp2_i, joyp3_i, joyp4_i})
	  .joyA(joyA),
	  .turboO(turbo)
	 );
	 
	 //Max
	 assign sram_ce_n = 1'b0;
	 assign sram_oe_n = 1'b0;
	 //assign joyp7_o = 1'b1;

///	joy_select <= hsync_aux;
   
  //Max
  joystick #(.CLK_MHZ(16'd84)) joy 
  (
 		.clk(clk_sys),
		.joyp1_i(joyp1_i),
		.joyp2_i(joyp2_i),
		.joyp3_i(joyp3_i),
		.joyp4_i(joyp4_i),
		.joyp6_i(joyp6_i),
		.joyp7_o(joyp7_o),
		.joyp9_i(joyp9_i),
		.joyOut(joyA),		// MXYZ SACB UDLR  1- On 0 - off
		.test(test)
  );
  
   /*joydecoder #(.FRECCLKIN(84), .FRECCLKOUT(16)) joydecoder
  (
     .clk        (clk_sys),
     .joy_data   (joy_data),
     .joy_clk    (joy_clk),
     .joy_load_n (joy_load_n), 
     .reset      (!locked),
     .hsync_n_s  (hsync),

     .joy1_o     (joyAmd_i),  // MXYZ SACB RLDU  Negative Logic
     .joy2_o     (joyBmd)   // MXYZ SACB RLDU  Negative Logic
   );*/   
  reg [3:0] frame_sync;
  reg frame;
  always @(posedge clk_sys)
  begin
    frame_sync <= {frame_sync[2:0],vsync};
	 if (frame_sync[3:2] == 2'b01) begin
	   frame <= !frame;
		joyAmd <= joyAmd_i;
    end
  end
  
  assign turbo_keys = {3{frame}} | {joyAmd[10],joyAmd[8],joyAmd[9]};
//Max
assign r[4:0] = video_r[7:3];
assign g[4:0] = video_g[7:3];
assign b[4:0] = video_b[7:3];
	 
//   multiboot vuelta_al_spectrum (
//    .clk_icap(clk7),
//    .REBOOT(master_reset)
//    );

//Max
	wire[15:0] turboLed;
	assign turboLed = (((turbo & 2'b10) != 2'b00) ? 16'd100 : ((turbo == 2'b01) ? 16'd50 : 16'd0 ));

	ledPWM ledRed(
		.nReset(1'b1),
		.clock(clk_sys),
		.enable(1'b1),
		.y1(16'd100),
		.y2(ledRedK),	
		.led(ledRedO)
    );

	ledPWM ledYellow(
		.nReset(1'b1),
		.clock(clk_sys),
		.enable(1'b1),
		.y1(turboLed),
		.y2(ledYellowK),	
		.led(ledYellowO)
    );

	ledPWM ledGreen(
		.nReset(1'b1),
		.clock(clk_sys),
		.enable(!sd_cs_n),
		.y1(16'd100),
		.y2(ledGreenK),	
		.led(ledGreenO)
    );

	ledPWM ledBlue(
		.nReset(1'b1),
		.clock(clk_sys),
		.enable(1'b0),
		.y1(16'd100),
		.y2(ledBlueK),	
		.led(ledBlueO)
    );

endmodule
