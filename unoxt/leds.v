`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: emax73
// 
// Create Date:    09:24:18 10/23/2021 
// Design Name: 
// Module Name:    leds 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: LEDs PWM Driver
//
// Dependencies: 
//
// Revision: 
// Revision 0.8 - File Created
// Additional Comments: 
// License: GPLv3
//
//////////////////////////////////////////////////////////////////////////////////
module ledPWM(
		input nReset,
		input clock,
		input enable, // Off/On
		input[15:0] y1, // 0-100% brightness1
		input[15:0] y2, // 0-100% brightness2
		output led // LED output
    );
	 
	 reg[15:0] cnt;
	 wire[15:0] pwm;
	 localparam period = 16'd10000;

	 assign pwm = (enable) ? (y1 * y2) : 16'd0;

	always @(posedge clock)
	begin
		if (!nReset)
				cnt <= 16'd0;
		else
		begin
			if (cnt < period - 16'd1)
				cnt <= cnt + 16'd1;
			else
				cnt <= 16'd0;
		end
	end
	
	reg ledO;
	always @(posedge clock)
	begin
		if (cnt < pwm)
			ledO <= 1'b1;
		else
			ledO <= 1'b0;
	end
	
	assign led = ledO;
	
endmodule
